from shallowgraver import policy_gradient, policy_model, environment

import argparse

if __name__ == '__main__':
	arg_parser = argparse.ArgumentParser (prog = 'train', description = 'A ShallowGraver tool used for model training')

	matrix_argument_group = arg_parser.add_argument_group ('matrix', 'properties of the matrices whose Graver bases will be calculated')

	matrix_argument_group.add_argument ('-m', '--matrix-height', type = int, default = 2, help = 'the width of the matrices to use')
	matrix_argument_group.add_argument ('-n', '--matrix-width', type = int, default = 4, help = 'the height of the matrices to use')
	matrix_argument_group.add_argument ('-v', '--min-entry-value', type = int, default = 0, help = 'the minimum entry value the matrices to use (inclusive)')
	matrix_argument_group.add_argument ('-V', '--max-entry-value', type = int, default = 5, help = 'the maximum entry value the matrices to use (inclusive)')

	train_argument_group = arg_parser.add_argument_group ('training', 'arguments relating to the training algorithm')

	train_argument_group.add_argument ('-e', '--epochs', type = int, default = 2500, help = 'number of training epochs')

	policy_model_argument_group = arg_parser.add_argument_group ('policy', 'arguments relating to the policy model')

	policy_model_argument_group.add_argument ('-i', '--policy-weights-input-file', type = str, default = None, help = 'the file to load the pretrained (baseline) policy model weights from')
	policy_model_argument_group.add_argument ('-o', '--policy-weights-output-file', type = str, default = None, help = 'the file to save the trained policy model weights in')
	policy_model_argument_group.add_argument ('--dropout', default = False, action = 'store_true', help = 'use dropout during training')
	policy_model_argument_group.add_argument ('--pooling', default = False, action = 'store_true', help = 'use pooling layers')

	args = arg_parser.parse_args ()

	env = environment.Environment (m = args.matrix_height, n = args.matrix_width, min_entry_value = args.min_entry_value, max_entry_value = args.max_entry_value)
	agent = policy_gradient.PolicyGradientAgent (env, policy_model.PolicyModel (dropout = args.dropout, pooling = args.pooling))

	if args.policy_weights_input_file is not None:
		agent.load_policy_model (args.policy_weights_input_file)

	agent.fit (epochs = args.epochs)

	if args.policy_weights_output_file is not None:
		agent.save_policy_model (args.policy_weights_output_file)