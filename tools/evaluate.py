from shallowgraver.policy_model import PolicyModel
from shallowgraver.environment import Environment

import argparse
import numpy as np
import random

if __name__ == '__main__':
	arg_parser = argparse.ArgumentParser (prog = 'train', description = 'A ShallowGraver tool used for model training')

	matrix_argument_group = arg_parser.add_argument_group ('matrix', 'properties of the matrices whose Graver bases will be calculated')

	matrix_argument_group.add_argument ('-m', '--matrix-height', type = int, default = 2, help = 'the width of the matrices to use')
	matrix_argument_group.add_argument ('-n', '--matrix-width', type = int, default = 4, help = 'the height of the matrices to use')
	matrix_argument_group.add_argument ('-v', '--min-entry-value', type = int, default = 0, help = 'the minimum entry value the matrices to use (inclusive)')
	matrix_argument_group.add_argument ('-V', '--max-entry-value', type = int, default = 5, help = 'the maximum entry value the matrices to use (inclusive)')

	train_argument_group = arg_parser.add_argument_group ('evaluation', 'arguments relating to evaluation')

	train_argument_group.add_argument ('-r', '--rollouts', type = int, default = 1000, help = 'number of rollouts during evaluation')

	policy_model_argument_group = arg_parser.add_argument_group ('policy', 'arguments relating to the policy model')

	policy_model_argument_group.add_argument ('-i', '--policy-weights-input-file', type = str, default = None, help = 'the file to save the trained policy model weights in')

	args = arg_parser.parse_args ()

	environment = Environment (m = args.matrix_height, n = args.matrix_width, min_entry_value = args.min_entry_value, max_entry_value = args.max_entry_value)
	policy_model = PolicyModel ()

	custom_model_reward = 0
	first_strategy_reward = 0
	random_strategy_reward = 0
	degree_strategy_reward = 0
	for rollout in range (1, args.rollouts + 1):
		print (f'Rollout {rollout}/{args.rollouts}')

		environment.reset ()

		if args.policy_weights_input_file is not None:
			while not environment.is_finished ():
				state = environment.get_state ()

				action_probabilities = policy_model (np.array ([state]))
				action = np.argmax (action_probabilities)
				environment.step (action)

			print (f'Reduction count for custom model: {-environment.get_total_reward ()}')
			custom_model_reward += environment.get_total_reward ()

			environment.reset (change_initial_state = False)
		
		while not environment.is_finished ():
			state = environment.get_state ()

			environment.step (0)

		print (f'Reduction count for the "first" strategy: {-environment.get_total_reward ()}')
		first_strategy_reward += environment.get_total_reward ()

		environment.reset (change_initial_state = False)

		while not environment.is_finished ():
			state = environment.get_state ()

			environment.step (random.randint (0, state.shape[0] - 1))

		print (f'Reduction count for the "random" strategy: {-environment.get_total_reward ()}')
		random_strategy_reward += environment.get_total_reward ()

		environment.reset (change_initial_state = False)

		while not environment.is_finished ():
			state = environment.get_state ()

			action = np.argmax (np.max (state, axis = 1))
			environment.step (action)

		print (f'Reduction count for the "degree" strategy: {-environment.get_total_reward ()}')
		degree_strategy_reward += environment.get_total_reward ()

	custom_model_reward /= args.rollouts
	first_strategy_reward /= args.rollouts
	random_strategy_reward /= args.rollouts
	degree_strategy_reward /= args.rollouts

	if args.policy_weights_input_file is not None:
		print (f'Mean reduction count for custom model across {args.rollouts} rollouts: {-custom_model_reward}')
	print (f'Mean reduction count for the "first" strategy across {args.rollouts} rollouts: {-first_strategy_reward}')
	print (f'Mean reduction count for the "random" strategy across {args.rollouts} rollouts: {-random_strategy_reward}')
	print (f'Mean reduction count for the "degree" strategy across {args.rollouts} rollouts: {-degree_strategy_reward}')
