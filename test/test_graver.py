from shallowgraver import pottier

import unittest
import numpy as np

def graver_basis_includes (half_graver: list, vec: np.array):
		for graver_elem in half_graver:
			if np.array_equal (vec, graver_elem) or np.array_equal (-vec, graver_elem):
				return True
			
		return False

class TestGraver (unittest.TestCase):
	#def test_333 (self):
	#	A = np.array ([
	#		[1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	#		[1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1],
	#		[1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1]
	#	], dtype=np.int32)
#
	#	print (nullspace.nullspace (A))
#
	#	#print (scipy.linalg.lapack.dgetrf (A))
	#	#print (scipy.linalg.null_space (A))

		#graver = pottier.half_graver_of_matrix (A)
		#print (graver)

		#self.assertEqual (len (graver), 795) #TODO
  
	#def test_bayer (self):
	#	lat = np.array ([
	#		[0, 4, 0, 0, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 4, 0, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 4, 0, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 4, 0, 0, 0],
	#		[0, 0, 0, 0, 0, 0, 0, 4, 0],
	#		[1, 1, 1, 0, 0, 0, 0, 0, 0],
	#		[-1, 0, 0, 1, 0, -1, 0, 1, 0],
	#		[0, -1, 0, -1, 1, 0, 0, 0, 1],
	#		[0, 0, -1, 0, -1, 1, 1, 0, 0]
	#	], dtype=np.int32)
	#	graver = pottier.half_graver_of_lattice (lat)
#
	#	print (graver)

	def test_hppi4 (self):
		A = np.array ([
			[1, 1, 1, 1],
			[1, 2, 3, 4]
		], dtype=np.int32)
		graver = pottier.half_graver_of_matrix (A)
  
		self.assertEqual (len (graver), 5)
		self.assertTrue (graver_basis_includes (graver, np.array ([1, -2, 1, 0], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([2, -3, 0, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, -1, -1, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([0, 1, -2, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, 0, -3, 2], dtype=np.int32)))
  
	def test_hppi5 (self):
		A = np.array ([
			[1, 1, 1, 1, 1],
			[1, 2, 3, 4, 5]
		], dtype=np.int32)
		graver = pottier.half_graver_of_matrix (A)
		
		self.assertEqual (len (graver), 16)
		self.assertTrue (graver_basis_includes (graver, np.array ([1, -2, 1, 0, 0], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([2, -3, 0, 1, 0], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([3, -4, 0, 0, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, -1, -1, 1, 0], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([2, -2, -1, 0, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, -1, 0, -1, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([0, 1, -2, 1, 0], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([0, 1, -1, -1, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, 0, -2, 0, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, -2, 0, 2, -1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([0, 0, 1, -2, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, 0, -1, -2, 2], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([0, 1, 0, -3, 2], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, 0, -3, 2, 0], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, 0, 0, -4, 3], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([0, 2, -3, 0, 1], dtype=np.int32)))
  
	def test_ppi3 (self):
		A = np.array ([
			[1, 2, 3]
		], dtype=np.int32)
		graver = pottier.half_graver_of_matrix (A)

		self.assertEqual (len (graver), 5)
		self.assertTrue (graver_basis_includes (graver, np.array ([2, -1, 0], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([3, 0, -1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, 1, -1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([1, -2, 1], dtype=np.int32)))
		self.assertTrue (graver_basis_includes (graver, np.array ([0, 3, -2], dtype=np.int32)))

	def test_trivial_kernel (self):
		A = np.array ([
			[2, 5, 11],
			[3, 7, 25],
			[1, 1, 1]
		], dtype=np.int32)
		graver = pottier.half_graver_of_matrix (A)

		self.assertEqual (len (graver), 0)

if __name__ == '__main__':
	unittest.main()