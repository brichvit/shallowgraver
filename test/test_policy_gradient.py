from shallowgraver.policy_gradient import PolicyGradientAgent
from shallowgraver.policy_model import PolicyModel

import unittest
import numpy as np
import tensorflow as tf
import gym

class GymEnvironment:
	def __init__ (self, inner_environment):
		self.inner_environment = inner_environment
		
		self.reset ()
		
	def is_finished (self):
		return self.finished
	
	def get_state (self):
		return self.state
	
	def reset (self):
		self.state = self.inner_environment.reset ()[0]
		self.finished = False

	def step (self, action):
		state, reward, terminated, truncated, _ = self.inner_environment.step (action.numpy ())
		self.state = state
		self.finished = terminated or truncated
		return reward

	@property
	def supports_value (self):
		return False

class TestPolicyGradient (unittest.TestCase):
	def test_cart_pole_0 (self):
		environment = GymEnvironment (gym.make ('CartPole-v0'))
		policy_model = tf.keras.Sequential ([
			tf.keras.layers.Dense (128, activation = 'relu'),
			tf.keras.layers.Dense (2, activation = 'softmax')
		])
		value_model = tf.keras.Sequential ([
			tf.keras.layers.Dense (128, activation = 'relu'),
			tf.keras.layers.Dense (1, activation = 'linear')
		])
		agent = PolicyGradientAgent (environment, policy_model, value_model = value_model)
		agent.fit (epochs = 25)

		no_eval_rollouts = 40
		total_loss = 0
		for i in range (no_eval_rollouts):
			total_loss += agent.rollout (max_length = 500)
		
		self.assertGreaterEqual (total_loss / no_eval_rollouts, 190)

if __name__ == '__main__':
	unittest.main()