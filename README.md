## ShallowGraver

This is a repository for the semestral work on the topic **Usage of reinforcement learning in Graver basis computation** for the NI-ADM (data mining algorithms) and NI-TNN (neural network theory) courses. The author is Vít Břichňáč ([brichvit@fit.cvut.cz](mailto:brichvit@fit.cvut.cz)).