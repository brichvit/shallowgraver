import numpy as np

rng = None

'''
Generate a random integer matrix of given size with entry values in the given range.
'''
def random_matrix (m: int, n: int, min_entry_value: int, max_entry_value: int, dtype: type = np.int32) -> np.array:
	global rng

	if rng is None:
		rng = np.random.default_rng ()
	
	return rng.integers (min_entry_value, max_entry_value, size = (m, n), dtype = dtype, endpoint = True)