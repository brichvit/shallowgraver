import tensorflow as tf
import numpy as np
import keras

'''
A policy model with the following architecture:

  /-------\                   /-------\                   /-------\                   /-------\
  | Input |   1D conv layer   |       |   1D conv layer   |       |      softmax      |Output |
  |       | ----------------> |p x 128| ----------------> | p x 1 | ----------------> |       |
  | p x n |  ReLU activation  |       | linear activation |       |                   | p x 1 |
  \-------/                   \-------/                   \-------/                   \-------/

Since the only trainable layers int the model are 1D convolutional layers, the model functions independently of the
input matrix height. This allows us to pass all actions from the state simultaneously and retrieve the their associated
probabilities (as per the policy) calculated using softmax.
'''
class PolicyModel (tf.keras.Model):
	def __init__ (self, dropout: bool = False, pooling: bool = False, epsilon: float = 1e-8):
		super ().__init__ ()

		self.dropout = dropout
		self.pooling = pooling
		self.epsilon = epsilon

		# We simulate 1D convolutional layers using dense layers applied on all rows independently
		self.conv1d_layer_1 = tf.keras.layers.TimeDistributed (tf.keras.layers.Dense (128, activation = 'relu', use_bias = True))
		self.conv1d_layer_2 = tf.keras.layers.TimeDistributed (tf.keras.layers.Dense (3 if pooling else 1, activation = None, use_bias = True, kernel_initializer = tf.keras.initializers.RandomUniform (minval = 0, maxval = 1)))

		self.flatten_layer = tf.keras.layers.Flatten ()
		self.softmax_layer = tf.keras.layers.Softmax ()

		if dropout:
			self.dropout_layer = tf.keras.layers.Dropout (0.3)

		if pooling:
			self.pooling1d_layer_1 = tf.keras.layers.MaxPool1D (pool_size = 3, data_format = 'channels_first')
			self.pooling1d_layer_2 = tf.keras.layers.MaxPool1D (pool_size = 3, data_format = 'channels_first')
	
	def call (self, X: tf.Tensor, training: bool = False) -> tf.Tensor:
		X = self.conv1d_layer_1 (X, training = training)
		if self.pooling:
			X = self.pooling1d_layer_1 (X, training = training)

		if self.dropout:
			X = self.dropout_layer (X, training = training)
		
		X = self.conv1d_layer_2 (X, training = training)
		if self.pooling:
			X = self.pooling1d_layer_2 (X, training = training)

		X = self.flatten_layer (X, training = training)

		# Simply using softmax on the self.flatten_layer output returns a sparse vector due to numerical stability issues
		# To address this, we apply the log-sum-exp trick
		# Source: https://cs231n.github.io/linear-classify/#softmax (see the 'Practical issues: Numeric stability' section)
		X -= tf.reduce_max (X)
		X = self.softmax_layer (X, training = training)

		# Clip the distribution minimum to be epsilon instead of zero
		X = self.epsilon + (1 - self.epsilon) * X
  
		return X