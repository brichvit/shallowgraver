import numpy as np
import tensorflow as tf

'''
Calculate discounted rewards from original rewards using a discount factor gamma.
'''
def discount_rewards (rewards: np.array, gamma: float):
	cumulative_reward = 0
	discounted_rewards = np.zeros_like (rewards, dtype = np.float32)

	for i in reversed (range (len (rewards))):
		cumulative_reward = rewards[i] + gamma * cumulative_reward
		discounted_rewards[i] = cumulative_reward
	
	return discounted_rewards

'''
Calculate the GAE (generalized advantage estimate) from the given rewards and values using a discount factor gamma * lamb.
'''
def generalized_advantage_estimate (rewards: np.array, values: np.array, gamma: float, lamb: float):
	delta = rewards - values
	
	# Add the previous value discounted by gamma to the current value
	delta[:-1] += gamma * values[1:]

	# Discount the rewards
	return discount_rewards (delta, gamma * lamb)

'''
Class used for building training datasets.
'''
class DatasetBuilder:
	'''
	Construct the dataset builder.
	'''
	def __init__ (self, gamma: float, lamb: float):
		self.gamma = gamma
		self.lamb = lamb

		self.states = []
		self.actions = []
		self.rewards = []
		self.action_probabilities = []
		self.values = []

		self.last_rollout_start_index = 0
		self.last_rollout_finish_index = 0

	'''
	Reset the dataset, thus losing all data added.
	'''
	def reset (self):
		self.states.clear ()
		self.actions.clear ()
		self.rewards.clear ()
		self.action_probabilities.clear ()
		self.values.clear ()

		self.last_rollout_start_index = 0
		self.last_rollout_finish_index = 0

	'''
	Add a single envoronment step into the dataset.
	'''
	def add_step (self, state, action, reward, action_probability, value):
		self.states.append (state)
		self.actions.append (action)
		self.rewards.append (reward)
		self.action_probabilities.append (action_probability)
		self.values.append (value)

		self.last_rollout_finish_index += 1
	
	'''
	Finish a rollout and calculate its discount rewards and generalized advantage estimates.
	'''
	def finish_rollout (self):
		last_rollout_slice = slice(self.last_rollout_start_index, self.last_rollout_finish_index)

		rewards = discount_rewards (np.array (self.rewards[last_rollout_slice]), self.gamma)
		values = generalized_advantage_estimate (
			np.array (self.rewards[last_rollout_slice]),
			np.array (self.values[last_rollout_slice]),
			self.gamma, self.lamb
		)

		self.rewards[last_rollout_slice] = rewards
		self.values[last_rollout_slice] = values

		self.last_rollout_start_index = self.last_rollout_finish_index

	'''
	Obtain the training dataset for the whole epoch. The dataset consists of all added steps since reset() was last called.
	'''
	def get (self, batch_size: int):
		actions = np.array (self.actions[:self.last_rollout_start_index], dtype = np.int32)
		action_probabilities = np.array (self.action_probabilities[:self.last_rollout_start_index], dtype = np.float32)
		advantages = np.array (self.values[:self.last_rollout_start_index], dtype = np.float32)
		values = np.array (self.rewards[:self.last_rollout_start_index], dtype = np.float32)

		# Normalize the advantages
		if np.std (advantages) > 1e-9:
			advantages = (advantages - np.mean (advantages)) / np.std (advantages)

		if self.states[0].ndim == 2:
			# Filter out all states with only one available action
			valid_state_indices = [i for i in range (self.last_rollout_start_index) if self.states[i].shape[0] > 1]
			states = [self.states[i].astype (np.int32) for i in valid_state_indices]
			actions = actions[valid_state_indices]
			action_probabilities = action_probabilities[valid_state_indices]
			advantages = advantages[valid_state_indices]
			values = values[valid_state_indices]

			state_max_size = np.array ([state.shape[0] for state in states], dtype = np.int32).max ()
			padded_states = []

			for state in states:
				if state.shape[0] == state_max_size:
					padded_states.append (state)
				else:
					padded_states.append (np.vstack ([state, -np.ones ((state_max_size - state.shape[0], state.shape[1]), dtype = np.int32)]))

			padded_states = np.array (padded_states)

			states_dataset = tf.data.Dataset.from_tensor_slices (padded_states)
			actions_dataset = tf.data.Dataset.from_tensor_slices (actions)
			action_probabilities_dataset = tf.data.Dataset.from_tensor_slices (action_probabilities)
			advantages_dataset = tf.data.Dataset.from_tensor_slices (advantages)
			values_dataset = tf.data.Dataset.from_tensor_slices (values)

			dataset = tf.data.Dataset.zip ((
				states_dataset,
				actions_dataset,
				action_probabilities_dataset,
				advantages_dataset,
				values_dataset
			))

			return dataset.batch (batch_size, drop_remainder = False)
		else:
			states = np.array (self.states[:self.last_rollout_start_index], dtype = np.float32)

			states_dataset = tf.data.Dataset.from_tensor_slices (states)
			actions_dataset = tf.data.Dataset.from_tensor_slices (actions)
			action_probabilities_dataset = tf.data.Dataset.from_tensor_slices (action_probabilities)
			advantages_dataset = tf.data.Dataset.from_tensor_slices (advantages)
			values_dataset = tf.data.Dataset.from_tensor_slices (values)

			dataset = tf.data.Dataset.zip ((
				states_dataset,
				actions_dataset,
				action_probabilities_dataset,
				advantages_dataset,
				values_dataset
			))

			return dataset.batch (batch_size, drop_remainder = False)

'''
Calculate the clipped proximal policy optimization loss function.
'''
def ppo_clipped_loss (new_action_log_probabilities, action_log_probabilities, advantages, epsilon = 0.2):
	min_adv = tf.where (advantages > 0, (1 + epsilon) * advantages, (1 - epsilon) * advantages)
	return -tf.minimum (tf.exp (new_action_log_probabilities - action_log_probabilities) * advantages, min_adv)

'''
Class representing the agent taking actions in the envorinment. The agent's policy model is trainable.
'''
class PolicyGradientAgent:
	'''
	Construct the agent.
	'''
	def __init__ (self, environment, policy_model, policy_learning_rate: float = 1e-4, policy_model_updates = 40, value_model = None, value_learning_rate: float = 1e-3, value_model_updates = 40, gamma: float = 0.99, lamb: float = 0.97, policy_divergence_limit = 1e-2):
		self.environment = environment
		self.policy_model = policy_model
		self.policy_optimizer = tf.keras.optimizers.Adam (learning_rate = policy_learning_rate)
		self.policy_model_updates = policy_model_updates

		self.value_model = value_model
		self.value_optimizer = tf.keras.optimizers.Adam (learning_rate = value_learning_rate)
		self.value_model_updates = value_model_updates

		self.policy_divergence_limit = policy_divergence_limit

		self.gamma = gamma
		self.dataset_builder = DatasetBuilder (gamma, lamb)

	'''
	Select (sample) the action to take using the policy model.
	'''
	def select_action (self, state: np.array):
		batch = tf.constant ([state])
		action_probabilities = self.policy_model (batch)

		# tf.math.log expects a vector of logits instead of the vector of probabilities
		action = tf.random.categorical (tf.math.log (action_probabilities), 1)[0,0]

		return action, action_probabilities[0, action]
	
	def estimate_value (self, state: np.array):
		return self.value_model (tf.constant ([state]))[0,0]

	'''
	Train the policy model for a given number of epochs.
	'''
	def fit (self, epochs: int, rollouts: int = 100, max_rollout_length: int = 500, batch_size: int = 64): #TODO: make batch size 128
		for i in range (epochs):
			self.dataset_builder.reset ()

			# Perform rollouts to calculate the rewards
			mean_rollout_reduction_count = 0
			for j in range (rollouts):
				mean_rollout_reduction_count += self.rollout (max_rollout_length)
			
			mean_rollout_reduction_count /= rollouts

			data = self.dataset_builder.get (batch_size = batch_size)

			# Fit the policy model
			mean_loss = self.fit_policy_model (data)
			if self.value_model is not None:
				self.fit_value_model (data)

			print (f'Epoch {i + 1}: loss {mean_loss}, mean reward: {mean_rollout_reduction_count}')

	'''
	Perform a training epoch on the policy model.
	'''
	def fit_policy_model (self, dataset):
		for policy_update in range (self.policy_model_updates):
			loss = 0
			kullback_leibler_divergence = 0
			batches = 0
			
			for batch in dataset:
				batch_loss, batch_kullback_leibler_divergence = self.fit_policy_model_step (states = batch[0], actions = batch[1], action_probabilities = batch[2], advantages = batch[3])

				loss += batch_loss
				kullback_leibler_divergence += batch_kullback_leibler_divergence
				batches += 1
			
			# Early stop if Kullback-Leibler divergence exceeds the policy divergence limit
			if self.policy_divergence_limit is not None and kullback_leibler_divergence > self.policy_divergence_limit:
				break

		#TODO: fix division by zero
		return 0 if batches == 0 else loss / batches

	'''
	Fit the policy model using a single batch.
	'''
	def fit_policy_model_step (self, states, actions, action_probabilities, advantages):
		action_log_probabilities = tf.math.log (action_probabilities)

		with tf.GradientTape () as tape:
			batch_action_probabilities = self.policy_model (states, training = True)
			batch_action_log_probabilities = tf.math.log (batch_action_probabilities)
			new_action_log_probabilities = tf.reduce_sum (tf.one_hot (actions, tf.shape(batch_action_log_probabilities)[1]) * batch_action_log_probabilities, axis = 1)

			loss = tf.reduce_mean (ppo_clipped_loss (new_action_log_probabilities, action_log_probabilities, advantages))

			kullback_leibler_divergence = tf.reduce_mean (action_log_probabilities - new_action_log_probabilities)
			
			# TODO: Add an option to apply bonus to distributions with larger entropy values
			#entropy = -tf.reduce_mean(new_action_log_probabilities)
			#loss -= self.entropy_bonus * entropy

		policy_params = self.policy_model.trainable_variables
		gradients = tape.gradient (loss, policy_params)
		self.policy_optimizer.apply_gradients (zip (gradients, policy_params))
		return loss, kullback_leibler_divergence
	
	def fit_value_model (self, dataset):
		loss = 0
		batches = 0
		
		for value_update in range (self.value_model_updates):
			for batch in dataset:
				batch_loss = self.fit_value_model_step (states = batch[0], values = batch[4])

				loss += batch_loss
				batches += 1
			
		#TODO: fix division by zero
		return 0 if batches == 0 else loss / batches
	
	def fit_value_model_step (self, states, values):
		with tf.GradientTape () as tape:
			predicted_values = tf.squeeze (self.value_model (states, training = True))
			loss = tf.reduce_mean (tf.keras.losses.mse (predicted_values, values))

		value_params = self.value_model.trainable_variables
		gradients = tape.gradient (loss, value_params)
		self.value_optimizer.apply_gradients (zip (gradients, value_params))

		return loss

	'''
	Save the weights of the policy model to a file.
	'''
	def save_policy_model (self, filename: str):
		self.policy_model.save_weights (filename)

	'''
	Load the weights of the policy model from a file.
	'''
	def load_policy_model (self, filename: str):
		self.policy_model.load_weights (filename).expect_partial ()

	'''
	Perform a rollout using the provided environment.
	'''
	def rollout (self, max_length):
		self.environment.reset ()

		total_reward = 0
		rollout_length = 0
		while not self.environment.is_finished () and rollout_length < max_length:
			state = self.environment.get_state ()

			action, action_probability = self.select_action (state)
			reward = self.environment.step (action)
			total_reward += reward
			
			# Obtain the value from the model
			value = 0

			if self.environment.supports_value:
				value = self.environment.value (self.gamma)
			if self.value_model is not None:
				value = self.estimate_value (state)

			self.dataset_builder.add_step (state, action, reward, action_probability, value)

			rollout_length += 1

		self.dataset_builder.finish_rollout ()

		return total_reward