from shallowgraver.nullspace import *

import numpy as np
import itertools
import heapq

def is_conformal (a: np.array, b: np.array) -> bool:
	return np.all (np.greater_equal (a * b, 0)) and np.all (np.less_equal (np.absolute (a), np.absolute (b)))

def normal_form (s: np.array, G: list) -> np.array:
	reduction_count = 0

	r = s.copy ()

	reduction_happened = True #TODO: cycle?
	while reduction_happened:
		reduction_happened = False

		for g in G:
			if is_conformal (g, r):
				reduction_happened = True
				reduction_count += 1

				r -= g
				break
			
			if is_conformal (-g, r):
				reduction_happened = True
				reduction_count += 1

				r += g
				break

	return r, reduction_count

def first_selection (_):
	return 0

def maximum_selection (vec):
	return -np.max (vec)

def pottier (G: np.array, selection_strategy, C: list = None, gamma: float = None) -> list:
	discount = 1
	discounted_return = 0

	i = 0

	if C is None:
		C = []

		for i1, i2 in itertools.combinations (range (len (G)), 2):
			f, g = G[i1], G[i2]

			heapq.heappush (C, (selection_strategy (f + g), i, f + g))
			i += 1
			heapq.heappush (C, (selection_strategy (f - g), i, f - g))
			i += 1
	else:
		C_list = C

		C = []
		for s in C_list:
			heapq.heappush (C, (selection_strategy (s), i, s))
			i += 1

	while C:
		_, _, s = heapq.heappop (C)
		r, reduction_count = normal_form (s, G)

		discounted_return += discount * reduction_count
		if gamma is not None:
			discount *= gamma

		if np.any (r):
			for g in G:
				heapq.heappush (C, (selection_strategy (r + g), i, r + g))
				i += 1
			
			G.append (r)
	
	return G, discounted_return

def half_graver_of_lattice (lat: np.array) -> list:
	reduced_lat = []
	for i in range (lat.shape[0]):
		reduced_lat.append (lat[i,:])

	return pottier (reduced_lat, None)[0]

def half_graver_of_matrix (A: np.array) -> list:
	kernel = nullspace (A)

	if kernel is None:
		return []

	G = [kernel[:,j].flatten () for j in range (kernel.shape[1])]

	return pottier (G, first_selection)[0]