import numpy as np

def idamax (A: np.array) -> int:
	return np.argmax (np.absolute (A))

'''
Calculate the LUP factorization (LU factorization with pivoting) of an m-by-n integer matrix A, where n >= m.

This is an integer reimplementation of the LUP algorithm from LAPACK
(https://netlib.org/lapack/explore-html/db/d4e/group__getf2_ga5f8f2998bad179dcee6a58d00599dba8.html#ga5f8f2998bad179dcee6a58d00599dba8),
which utilizes multiplication instead of division in order to preserve the integer entries.
'''
def lu_with_pivoting(A: np.array) -> tuple:
	# Get the number of rows and columns
	m, n = A.shape

	divisors = np.zeros (m - 1, dtype = A.dtype)
	
	for j in range (min (m, n)):
		jp = j + idamax (A[j:, j])

		if A[jp, j] != 0:
			if jp != j:
				# Swap the rows
				A[j, :], A[jp, :] = A[jp, :].copy (), A[j, :].copy ()
			
			if j < m - 1:
				# "Divide" the jth column (this is done by multiplying other columns)
				divisors[j] = A[j,j]
				A[j+1:,:j] *= divisors[j]
				A[j+1:,j+1:] *= divisors[j]
		
		if j < min (m, n) - 1:
			# Update trailing submatrix
			A[j+1:, j+1:] -= A[j+1:, j].reshape (-1, 1) @ A[j, j+1:].reshape (1, -1)

	divisors_prod = 1

	for j in range (m-2, -1, -1):
		if divisors[j] != 0:
			A[0:j+1,:] *= divisors[j]
			divisors_prod *= divisors[j]

	# Explicitly construct the L & U matrices
	L = divisors_prod * np.eye (m, dtype = A.dtype)
	for i in range (m):
		L[i+1:, i] = A[i+1:, i]

	for i in range (m):
		A[i+1:, i] = 0

	return L, A


'''
Calculate the basis of the nullspace of an m-by-n integer matrix in row echelon form (REF), where n >= m.

Instead of using division, this function utilizes multiplication so that the entries of the resulting matrix
are integer.
'''
def nullspace_of_ref_matrix (A: np.array) -> np.array:
	m, n = A.shape

	constrained_var_indices = []
	free_var_indices = []
	j = 0

	for i in range (m):
		while j < n and A[i, j] == 0:
			free_var_indices.append (j)
			j += 1

		if j == n:
			# Singular matrix, cut off the trailing zero rows
			A = A[0:i]
			m = i
			break
		
		constrained_var_indices.append (j)

		j += 1
	
	while j < n:
		free_var_indices.append (j)
		j += 1

	nullspace = np.zeros ((n, n - m), dtype = A.dtype)

	for k in range (n - m):
		free_var_index = free_var_indices[k]

		nullspace[free_var_index, k] = 1

		for i in range (m-1, -1, -1):
			constrained_var_index = constrained_var_indices[i]

			dot_prod = A[i, free_var_index] * nullspace[free_var_index,k]
			for l in range (i + 1, m):
				dot_prod += nullspace[constrained_var_indices[l],k] * A[i, constrained_var_indices[l]]
			
			# "Divide" the k-th column by A[i,constrained_var_index] (this is done by multiplying other columns)
			nullspace[:, k] *= A[i,constrained_var_index]

			nullspace[constrained_var_index, k] = -dot_prod

	# Reduce all entries of nullspace by their GCD
	nullspace //= np.gcd.reduce (nullspace)
			
	return nullspace

'''
Calculate the basis of the nullspace of a general m-by-n integer matrix, where n >= m.

The entries of the resulting basis vectors are integers.
'''
def nullspace (A: np.array) -> np.array:
	m, n = A.shape

	if m > n:
		raise ValueError ('nullspace is only supported for matrices with m <= n')

	# Perform LU factorization with pivoting
	L, U = lu_with_pivoting (A)

	# Solve Ux = 0
	return nullspace_of_ref_matrix (U)