from shallowgraver.matrixtools import random_matrix
from shallowgraver.nullspace import nullspace
from shallowgraver.pottier import pottier, is_conformal, maximum_selection

import numpy as np
import itertools

'''
The reinforcement learning environment based on the Pottier algorithm, where:
- the action set corresponds to the vector set C
- each state is fully determined by the action set (and as such, fully observable)
- the (only) final state has an empty action set
- the reward for every action is -1 (we want to minimize the number of reductions performed)
'''
class Environment:
	'''
	Construct the environment that generates matrices of a given size and entry value range as training data.
	'''
	def __init__(self, matrix_generator = random_matrix, **kwargs):
		self.matrix_generator = matrix_generator
		self.matrix_generator_kwargs = kwargs
		
		self.reset ()

	'''
	Reset the environment, either by generating a new initial state or returning to the last one.
	'''
	def reset (self, change_initial_state = True):
		self.total_reward = 0

		if change_initial_state:
			mat = self.matrix_generator (**self.matrix_generator_kwargs)
			kernel = nullspace (mat)

			self.G = [kernel[:,j].flatten () for j in range (kernel.shape[1])]

			self.state = []

			for i1, i2 in itertools.combinations (range (len (self.G)), 2):
				f, g = self.G[i1], self.G[i2]

				self.state.append (f + g)
				self.state.append (f - g)
			
			self.last_initial_state = self.state.copy ()
			self.last_initial_G = self.G.copy ()
		else:
			self.state = self.last_initial_state.copy ()
			self.G = self.last_initial_G.copy ()
	
	'''
	Retrieve the current state.
	'''
	def get_state (self):
		return np.array (self.state, dtype = np.float32)

	'''
	Retrieve the total reward for the current rollout.
	'''
	def get_total_reward (self):
		return self.total_reward
	
	'''
	Check whether the current rollout is finished (which is determined by the current action set being empty).
	'''
	def is_finished (self):
		return len (self.state) == 0
	
	'''
	Reduce a vector to normal form using a set of vectors G.
	'''
	def normal_form (self, s: np.array) -> np.array:
		reduction_count = 0

		r = s.copy ()

		reduction_happened = True #TODO: cycle?
		while reduction_happened:
			reduction_happened = False

			for g in self.G:
				if is_conformal (g, r):
					reduction_happened = True
					reduction_count += 1

					r -= g
					break
				
				if is_conformal (-g, r):
					reduction_happened = True
					reduction_count += 1

					r += g
					break

		return r, reduction_count
	
	'''
	Take a step (reduce one vector to normal form & add its sums with every vector from G to G if it is not zero)
	using the selected action.
	'''
	def step (self, action_index: int):
		# TODO: make an option for reduction-based reward

		s = self.state.pop (action_index)

		r, reduction_count = self.normal_form (s)

		if np.any (r):
			for g in self.G:
				self.state.append (r + g)
			
			self.G.append (r)

		reward = -1.0 - reduction_count
		self.total_reward += reward

		return reward

	def value (self, gamma: float):
		_, value = pottier (self.G, selection_strategy = maximum_selection, C = self.state, gamma = gamma)
		return value

	@property
	def supports_value (self):
		return True
